Rails.application.routes.draw do
  resources :articles
  resources :type_articles
  # resources :article
  # resources :article_types
  resources :categories
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

  get 'weathers', to: 'weathers#view_data'
  
  post 'auth/login', to: 'authentications#authenticate'
  post 'signup', to: 'users#create'

  get 'users', to: 'users#index'
  get 'user/:id', to: 'users#show'
  put 'user/:id', to: 'users#update'
  delete 'user/delete/:id', to: 'users#destroy'

  post 'article/create', to: 'articles#create'

end
