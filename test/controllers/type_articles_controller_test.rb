require 'test_helper'

class TypeArticlesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @type_article = type_articles(:one)
  end

  test "should get index" do
    get type_articles_url, as: :json
    assert_response :success
  end

  test "should create type_article" do
    assert_difference('TypeArticle.count') do
      post type_articles_url, params: { type_article: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show type_article" do
    get type_article_url(@type_article), as: :json
    assert_response :success
  end

  test "should update type_article" do
    patch type_article_url(@type_article), params: { type_article: {  } }, as: :json
    assert_response 200
  end

  test "should destroy type_article" do
    assert_difference('TypeArticle.count', -1) do
      delete type_article_url(@type_article), as: :json
    end

    assert_response 204
  end
end
