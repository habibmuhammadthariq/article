class Article < ApplicationRecord
  belongs_to :user
  belongs_to :category
  belongs_to :type_article
end
