class User < ApplicationRecord
  has_many :articles
  enum role: { admin: 0 }
  has_secure_password

  validates_presence_of :name, :email, :password_digest, :bod, :role
  validates :email, uniqueness: true
end
