require 'sidekiq-scheduler'

class WeatherWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform
    puts "[v1] get weather data from openweathermap"

    WeathersController.get_weather_data()

    puts "#{Time.now} - Success!"
  end
end
