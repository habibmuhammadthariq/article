class ItemsController < ApplicationController
  before_action :set_note
  before_action :set_note_item, only: [:show, :update, :destroy]

  # GET /notes/:note_id/items
  def index
    json_response(@note.items)
  end

  # GET /notes/:note_id/items/:id
  def show
    json_response(@item)
  end

  # POST /notes/:note_id/items
  def create
    @note.items.create!(item_params)
    json_response(@note, :created)
  end

  # PUT /notes/:note_id/items/:id
  def update
    @item.update(item_params)
    head :no_content
  end

  # DELETE /notes/:note_id/items/:id
  def destroy
    @item.destroy
    head :no_content
  end

  private

  def item_params
    params.permit(:name, :done)
  end

  def set_note
    @note = Note.find(params[:note_id])
  end

  def set_note_item
    @item = @note.items.find_by!(id: params[:id]) if @note
  end
end
