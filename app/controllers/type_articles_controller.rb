class TypeArticlesController < ApplicationController
  before_action :set_type_article, only: [:show, :update, :destroy]

  # GET /type_articles
  def index
    @type_articles = TypeArticle.all

    render json: @type_articles
  end

  # GET /type_articles/1
  def show
    render json: @type_article
  end

  # POST /type_articles
  def create
    # @type_article = TypeArticle.new(type_article_params)

    # if @type_article.save
    #   render json: @type_article, status: :created, location: @type_article
    # else
    #   render json: @type_article.errors, status: :unprocessable_entity
    # end

    @type_article = TypeArticle.new(type_article_params)
    if @type_article.save
      render json: { status: "Succes", 
      message: "type of article already post", result: @type_article}, status: :created #201
    else
      render json: {status: "Failed", 
      message: "type of article can't dipost"},status: :unprocessable_entity #status code
    end
  end

  # PATCH/PUT /type_articles/1
  def update
    if @type_article.update(type_article_params)
      render json: @type_article
    else
      render json: @type_article.errors, status: :unprocessable_entity
    end
  end

  # DELETE /type_articles/1
  def destroy
    @type_article.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_type_article
      @type_article = TypeArticle.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def type_article_params
      # params.fetch(:type_article, :name)
      params.permit(:name)
    end
end
