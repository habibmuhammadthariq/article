class UsersController < ApplicationController
  skip_before_action :authorize_request, only: :create
  before_action :set_user, only: [:show, :update, :destroy]

  def create
    user = User.create(create_params)
    auth_token = AuthenticateUser.new(user.email, user.password).call
    response = { message: Message.account_created, auth_token: auth_token }
    render json: response, status: :created
  end

  def index 
    @users = User.all
    render json: @users 
  end

  def show 
    render json: @role
  end

  def update
    @user.update(create_params)
  end

  def destroy
    @user.destroy
  end

  private

  def authenticate(email, password)
    command = AuthenticateUser.call(email, password)

    if command.success?
      render json: {
        access_token: command.result,
        message: 'Login Successful'
      }
    else
      render json: { error: command.errors }, status: :unauthorized
    end
  end

  def set_user 
    @user = User.find(params[:id])
  end

  def create_params
    params.permit(
      :name, 
      :email,
      :role, 
      :password,
      :bod
    )
  end
end
