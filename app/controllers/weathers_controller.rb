class WeathersController < ApplicationController
  skip_before_action :authorize_request

  def self.get_weather_data
    url = 'https://samples.openweathermap.org/data/2.5/weather?id=2172797&appid=b6907d289e10d714a6e88b30761fae22'
    response = HTTParty.get(url)
    self.store_request(response)
    render json: response, status: :ok
  end

  def self.store_request(response)
    Weather.create(data: response)
  end

  def view_data
    weather = Weather.last
    current_weather = weather ? {weather: weather[:data]['weather'], name: weather[:data]['name']} : {weather: 'data not found', name: 'data not found'}
    render json: {data: current_weather}
  end
end
