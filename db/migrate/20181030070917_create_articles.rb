class CreateArticles < ActiveRecord::Migration[5.2]
  def change
    create_table :articles do |t|
      t.string :title
      t.string :content
      t.integer :category_id
      t.integer :type_id
      t.integer :user_id

      t.timestamps
    end
  end
end
